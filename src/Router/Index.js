import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import Login from "../Pages/Login";
import Register from "../Pages/Register";
import Profile from "../Pages/Profile";
import Learn from "../Pages/Learn";
import Statistic from "../Pages/Statistic";
import SplashScreen from "../Pages/SplashScreen";
import Chat from "../Pages/Chat";
import JavaScript from "../Pages/JavaScript";
import Flutter from "../Pages/Flutter";
import ReactNative from "../Pages/ReactNative";
import Java from "../Pages/Java";
import Python from "../Pages/Python";
import WebDasar from "../Pages/WebDasar";
const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

const Index = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SplashScreen">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Learn"
          component={Learn}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Statistic"
          component={Statistic}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MainApp"
          component={MainApp}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Chat"
          component={Chat}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="JavaScript"
          component={JavaScript}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Flutter"
          component={Flutter}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ReactNative"
          component={ReactNative}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Java"
          component={Java}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Python"
          component={Python}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="WebDasar"
          component={WebDasar}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


const MainApp = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          height: 75,
          backgroundColor: "#3FA9D7",
          position: "absolute",
          bottom: 5,
          left: 20,
          right: 20,
          elevation: 0,
          borderRadius: 15,
        },
        tabBarShowLabel: false,
      }}
    >
      <Tab.Screen
        name="Learn"
        component={Learn}
        options={{
          tabBarLabel: "Learn",
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 2,
              }}
            >
              <Image
                source={require("../assets/book-black.png")}
                style={{
                  width: 30,
                  height: 30,
                  tintColor: focused ? "#EEEEE4" : "black",
                }}
              />
              <Text style={{ color: focused ? "#EEEEE4" : "black" }}>
                Learn
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Statistic"
        component={Statistic}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 2,
              }}
            >
              <Image
                source={require("../assets/stats-black.png")}
                style={{
                  width: 30,
                  height: 30,
                  tintColor: focused ? "#EEEEE4" : "black",
                }}
              />
              <Text style={{ color: focused ? "#EEEEE4" : "black" }}>
                Statistic
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 2,
              }}
            >
              <Image
                source={require("../assets/profile-black.png")}
                style={{
                  width: 30,
                  height: 30,
                  tintColor: focused ? "#EEEEE4" : "black",
                }}
              />
              <Text style={{ color: focused ? "#EEEEE4" : "black" }}>
                Profile
              </Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop: 2,
              }}
            >
              <Image
                source={require("../assets/messenger.png")}
                style={{
                  width: 30,
                  height: 30,
                  tintColor: focused ? "#EEEEE4" : "black",
                }}
              />
              <Text style={{ color: focused ? "#EEEEE4" : "black" }}>
                Mentor
              </Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};




export default Index;

const styles = StyleSheet.create({});
