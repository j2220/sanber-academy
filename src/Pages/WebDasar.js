import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const WebDasar = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30 }}>Web Dasar</Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text
          style={{
            textAlign: "justify",
            fontSize: 18,
            lineHeight: 30,
            color: "black",
          }}
        >
          Website di era sekarang sudah menjadi kebutuhan utama yang tidak bisa
          diabaikan. Seluruh sektor bisnis atau edukasi dapat memanfaatkan
          website sebagai alat untuk promosi, tukar informasi, dan lainnya.
          Bedasarkan data dari World Wide Web Technology Surveys, dari seluruh
          website yang aktif, 88.2% menggunakan HTML, 95.6% menggunakan CSS dan
          95% menggunakan JavaScript. Kelas ini membahas tuntas dasar HTML, CSS
          dan JavaScript sebagai tiga fondasi pembuatan website.
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={require("../assets/web.png")}
          style={{ width: 100, height: 100 }}
        />
      </View>
    </View>
  );
};

export default WebDasar

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#CD0000",
  },
});