// @refresh
import {
  StyleSheet,
  Text,
  View,
  LogBox,
  TextInput,
  Button,
} from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
import React, { useEffect, useState, useCallback } from "react";
import firebase from "firebase/compat/app";
import AsyncStorage from "@react-native-async-storage/async-storage";
import "firebase/compat/firestore";
import "firebase/compat/auth";
import { async } from "@firebase/util";
// FireBase Config
const firebaseConfig = {
  apiKey: "AIzaSyAhp86E3z2R8qOcwzlqI1c_HnmxYbbrim8",
  authDomain: "authfirebasern-1fa10.firebaseapp.com",
  projectId: "authfirebasern-1fa10",
  storageBucket: "authfirebasern-1fa10.appspot.com",
  messagingSenderId: "363816926304",
  appId: "1:363816926304:web:6779a82537bc5aedd8d705",
  measurementId: "G-05WLEJ7027",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
LogBox.ignoreLogs(["Setting a timer for a long period of time"]);
// FireStore
const db = firebase.firestore();
const chatsRef = db.collection("chats");

const Chat = () => {
  const [user, setUser] = useState(null);
  const [name, setName] = useState("");
  const [messages, setMessages] = useState([]);
//   Get Messages
  useEffect(() => {
    readUser();
    const unsubscribe = chatsRef.onSnapshot((querySnapshot) => {
      const messageFirestore = querySnapshot
        .docChanges()
        .filter(({ type }) => type === "added")
        .map(({ doc }) => {
          const message = doc.data();
          return { ...message, createdAt: message.createdAt.toDate() };
        })
        .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
      appendMessages(messageFirestore);
    });
    return () => unsubscribe();
  }, []);
// Pesan agar tidak terpumpuk
const appendMessages = useCallback((messages) => {
  setMessages((previousMessages) =>
    GiftedChat.append(previousMessages, messages)
  );
}, [messages]);

// Read User, mencoba menggunakan uid auth masih belum dapet :D
  async function readUser() {
    const user = await AsyncStorage.getItem("user");
    if (user) {
      setUser(JSON.parse(user));
    }
  }
  async function handlePress() {
    const _id = firebase.auth().currentUser.uid;
    setName(firebase.auth().currentUser.uid)
    const user = { _id, name };
    // await AsyncStorage.setItem("user", JSON.stringify(user));
    setUser(user);
  }

//   Send Message
  async function handleSend(messages) {
    const write = messages.map((m) => chatsRef.add(m));
    await Promise.all(write);
  }

//   Jika Tidak ada User, get paksa dari Uid Auth
  if (!user) {
    return (
    handlePress()
    );
  }

  return (
    <>
      <GiftedChat messages={messages} user={user} onSend={handleSend} />
      <View style={{ marginBottom: 100 }} />
    </>
  );
};

export default Chat;

const styles = StyleSheet.create({});
