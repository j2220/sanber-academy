import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";

const Statistic = ({ navigation}) => {
  return (
    <View style={styles.container}>
      {/* Header */}
      <View
        style={{
          flexDirection: "row",
          marginTop: 60,
          marginLeft: -10,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <View>
          <Text style={{ fontSize: 36, marginTop: 21 }}>Statistik</Text>
        </View>
      </View>
      {/* Fitur */}
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            marginLeft: 20,
            marginRight: 20,
            marginTop: 34,
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: "#F4DC1C",
              height: 155,
              width: 150,
              borderRadius: 10,
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              JavaScript
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/javascript.png")}
              style={{ marginTop: 10, marginLeft: 0 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "#CACAC2",
              height: 155,
              width: 160,
              borderRadius: 10,
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              Flutter
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/flutter.png")}
              style={{ marginTop: 27, marginLeft: -5 }}
            />
          </TouchableOpacity>
        </View>
        {/* 2 */}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            marginLeft: 20,
            marginRight: 20,
            marginTop: 20,
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: "#44A4F4",
              height: 155,
              width: 160,
              borderRadius: 10,
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
              }}
            >
              React Native
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/react-native.png")}
              style={{ marginTop: 21, marginLeft: 8 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "#000000",
              height: 155,
              width: 160,
              borderRadius: 10,
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
              }}
            >
              Java
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
                color: "white",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/java.png")}
              style={{ marginTop: 18, marginLeft: 8 }}
            />
          </TouchableOpacity>
        </View>
        {/* 3 */}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            marginLeft: 20,
            marginRight: 20,
            marginTop: 20,
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: "#4C8478",
              height: 155,
              width: 160,
              borderRadius: 10,
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
              }}
            >
              Python
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 28,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
                color: "white",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/python.png")}
              style={{ marginTop: 28, marginLeft: 8 }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "#CD0000",
              height: 155,
              width: 160,
              borderRadius: 10,
              color: "white",
            }}
          >
            <Text
              style={{
                paddingTop: 5,
                paddingLeft: 10,
                fontSize: 20,
                fontWeight: "bold",
                color: "white",
              }}
            >
              Web Dasar
            </Text>
            <Text
              style={{
                paddingLeft: 10,
                fontSize: 28,
                fontSize: 30,
                paddingTop: 16,
                fontWeight: "bold",
                color: "white",
              }}
            >
              80/100
            </Text>
            <Image
              source={require("../assets/web.png")}
              style={{ marginTop: 28, marginLeft: 8 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Statistic;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});
