import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React, { useState, useEffect } from "react";
import { initializeApp } from "firebase/app";
import { useNavigation } from "@react-navigation/native";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const Register = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const firebaseConfig = {
    apiKey: "AIzaSyAhp86E3z2R8qOcwzlqI1c_HnmxYbbrim8",
    authDomain: "authfirebasern-1fa10.firebaseapp.com",
    projectId: "authfirebasern-1fa10",
    storageBucket: "authfirebasern-1fa10.appspot.com",
    messagingSenderId: "363816926304",
    appId: "1:363816926304:web:6779a82537bc5aedd8d705",
    measurementId: "G-05WLEJ7027",
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const submit = () => {
    const data = {
      email,
      password,
    };
    console.log(data);
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        // console.log("Register Berhasil!")
        alert("Registarasi Berhasil!\nSilahkan untuk Login Ulang!");
        navigation.navigate("Login");
      })
      .catch(() => {
        alert("Registrasi Gagal");
      });
  };
  // // Initialize Firebase
  return (
    <View style={styles.container}>
      {/* Gambar */}
      <View
        style={styles.gambar}
        style={{
          justifyContent: "center",
          alignItems: "center",
          marginTop: 50,
        }}
      >
        <Image source={require("../assets/sanber-code.png")} />
        <Text
          style={{
            color: "#073665",
            fontWeight: "bold",
            fontSize: 30,
            marginTop: -20,
          }}
        >
          Academy
        </Text>
      </View>
      {/* Form */}
      <View style={styles.form}>
        <View
          style={{
            marginTop: 43,
            paddingVertical: 10,
            width: 300,
            marginLeft: 6,
          }}
        >
          <Text style={{ color: "#073665", fontSize: 24 }}>Daftar</Text>
        </View>

        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 15,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
            fontSize: 18,
          }}
          placeholder="Username/Email"
          value={email}
          onChangeText={(value) => {
            setEmail(value);
          }}
        />

        <TextInput
          secureTextEntry={true}
          style={{
            marginTop: 8,
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 15,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
            fontSize: 18,
          }}
          placeholder="Password"
          value={password}
          onChangeText={(value) => {
            setPassword(value);
          }}
        />
        <View
          style={{
            marginTop: -14,
            paddingVertical: 10,
            width: 300,
          }}
        >
          <TouchableOpacity>
            <Text style={{ fontSize: 13, marginLeft: 6 }}>
              Sudah mempunyai Akun ? Klik{" "}
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 16,
                  textAlign: "center",
                }}
              >
                Masuk
              </Text>
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* Button */}
      <View style={styles.button}>
        <View>
          <TouchableOpacity
            style={{
              marginRight: 80,
              marginLeft: 80,
              marginTop: 10,
              paddingTop: 10,
              paddingBottom: 10,
              backgroundColor: "#517DA2",
              borderRadius: 15,
              borderWidth: 1,
              borderColor: "#fff",
            }}
            onPress={submit}
          >
            <Text style={{ color: "#fff", textAlign: "center", fontSize: 20 }}>
              Daftar
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack("Login");
            }}
            style={{
              marginRight: 80,
              marginLeft: 80,
              marginTop: 10,
              paddingTop: 10,
              paddingBottom: 10,
              backgroundColor: "#fff",
              borderRadius: 15,
              borderWidth: 1,
              borderColor: "#517DA2",
            }}
          >
            <Text
              style={{ color: "#517DA2", textAlign: "center", fontSize: 20 }}
            >
              Masuk
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};;

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  form: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    marginTop: 55,
  },
});
