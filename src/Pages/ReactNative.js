import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const ReactNative = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30 }}> React Native</Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text
          style={{
            textAlign: "justify",
            fontSize: 18,
            lineHeight: 30,
            color: "black",
          }}
        >
          React Native adalah sebuah framework berbasis JavaScript yang
          digunakan untuk mengembangkan aplikasi mobile di dua sistem operasi
          secara bersamaan, yaitu Android dan iOS. React Native sendiri pertama
          kali diluncurkan pada tahun 2015 oleh Facebook dan bersifat open
          source. Dalam membuat sebuah aplikasi, kamu harus mempelajari sebuah
          bahasa pemrograman yang spesifik atau khusus digunakan untuk sebuah
          platform.
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={require("../assets/react-native.png")}
          style={{ width: 100, height: 110 }}
        />
      </View>
    </View>
  );
};

export default ReactNative

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#44A4F4",
  },
});