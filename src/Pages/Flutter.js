import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'

const Flutter = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30 }}> Flutter</Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text style={{ textAlign: "justify", fontSize: 18, lineHeight: 30 }}>
          Flutter adalah platform yang digunakan para developer untuk membuat
          aplikasi multiplatform hanya dengan satu basis coding (codebase).
          Artinya, aplikasi yang dihasilkan dapat dipakai di berbagai platform,
          baik mobile Android, iOS, web, maupun desktop.
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={require("../assets/flutter.png")}
          style={{ width: 120, height: 70 }}
        />
      </View>
    </View>
  );
}

export default Flutter

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#CACAC2",
  },
});