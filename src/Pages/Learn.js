import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import React, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
// import Login from "./Login";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const Learn = ({ navigation }) => {
  const [user, setUser] = useState({});
  useEffect(() => {
    const userInfo = firebase.auth().currentUser;
    setUser(userInfo);
  }, []);

  const firebaseConfig = {
    apiKey: "AIzaSyAhp86E3z2R8qOcwzlqI1c_HnmxYbbrim8",
    authDomain: "authfirebasern-1fa10.firebaseapp.com",
    projectId: "authfirebasern-1fa10",
    storageBucket: "authfirebasern-1fa10.appspot.com",
    messagingSenderId: "363816926304",
    appId: "1:363816926304:web:6779a82537bc5aedd8d705",
    measurementId: "G-05WLEJ7027",
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }

  const moveToJavaScript = () => {
    navigation.navigate("JavaScript")
  };
  const moveToFlutter = () => {
    navigation.navigate("Flutter")
  };
  const moveToReactNative = () => {
    navigation.navigate("ReactNative");
  };
  const moveToJava = () => {
    navigation.navigate("Java")
  };
  const moveToPython = () => {
    navigation.navigate("Python")
  };
  const moveToWebDasar = () => {
    navigation.navigate("WebDasar")
  };

  return (
    <View style={styles.container}>
      {/* Header */}
      <View style={styles.header}>
        <View>
          <Text style={{ fontSize: 15, textAlign: "right", marginRight: 15 }}>
            👋 Hi, {user.email}
          </Text>
        </View>
        <View>
          <Text style={styles.headerText}>Pilih Materi Kamu</Text>
        </View>
      </View>
      {/* Fitur */}
      <View>
        <View style={styles.fitur1}>
          <TouchableOpacity
            onPress={moveToJavaScript}
            style={styles.fiturJavaScript}
          >
            <Text style={styles.fiturJavaScriptJudul}>JavaScript</Text>
            <Text style={styles.fiturJavaScriptDesc}>
              Mempelajari dasar sampai mahir JavaScript ES6
            </Text>
            <Image
              source={require("../assets/javascript.png")}
              style={styles.fiturJavaScriptImage}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={moveToFlutter} style={styles.fiturFlutter}>
            <Text style={styles.fiturFlutterJudul}>Flutter</Text>
            <Text style={styles.fiturFlutterDesc}>
              Mempelajari untuk membuat aplikasi mobile dengan Flutter
            </Text>
            <Image
              source={require("../assets/flutter.png")}
              style={styles.fiturFlutterImage}
            />
          </TouchableOpacity>
        </View>
        {/* 2 */}
        <View style={styles.fitur2}>
          <TouchableOpacity onPress={moveToReactNative} style={styles.fiturRN}>
            <Text style={styles.fiturRNJudul}>React Native</Text>
            <Text style={styles.fiturRNDesc}>
              Mempelajari untuk membuat aplikasi mobile dengan React Native
            </Text>
            <Image
              source={require("../assets/react-native.png")}
              style={styles.fiturRNImage}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={moveToJava} style={styles.fiturJava}>
            <Text style={styles.fiturJavaJudul}>Java</Text>
            <Text style={styles.fiturJavaDesc}>
              Mempelajari dasar sampai mahir Java
            </Text>
            <Image
              source={require("../assets/java.png")}
              style={styles.fiturJavaImage}
            />
          </TouchableOpacity>
        </View>
        {/* 3 */}
        <View style={styles.fitur3}>
          <TouchableOpacity onPress={moveToPython} style={styles.fiturPython}>
            <Text style={styles.fiturPythonJudul}>Python</Text>
            <Text style={styles.fiturPythonDesc}>
              Mempelajari dasar sampai mahir Python
            </Text>
            <Image
              source={require("../assets/python.png")}
              style={styles.fiturPythonImage}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={moveToWebDasar}
            style={styles.fiturWebDasar}
          >
            <Text style={styles.fiturWebDasarJudul}>Web Dasar</Text>
            <Text style={styles.fiturWebDasarDesc}>
              Mempelajari dasar pembuatan WEB dengan HTML, CSS, dan JavaScript
            </Text>
            <Image
              source={require("../assets/web.png")}
              style={styles.fiturWebDasarImage}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Learn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },

  // Header
  header: {
    // flexDirection: "row",
    marginTop: 50,
    marginLeft: -10,
    // alignItems: "center",
    // justifyContent: "center",
  },
  headerText: {
    fontSize: 30,
    marginTop: 20,
    // backgroundColor: "green",
    textAlign: "center",
    // fontWeight: "bold"
  },

  // Fitur 1
  // Fitur JavaScript
  fitur1: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 32,
  },
  fiturJavaScript: {
    backgroundColor: "#F4DC1C",
    height: 155,
    width: 150,
    borderRadius: 10,
  },
  fiturJavaScriptJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  fiturJavaScriptDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
  },
  fiturJavaScriptImage: {
    marginTop: 9,
    marginLeft: 0,
  },

  // Fitur Flutter
  fiturFlutter: {
    backgroundColor: "#CACAC2",
    height: 155,
    width: 160,
    borderRadius: 10,
  },
  fiturFlutterJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  fiturFlutterDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
  },
  fiturFlutterImage: {
    marginTop: 26,
    marginLeft: -5,
  },

  // Fitur 2
  // Fitur React Native
  fitur2: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
  },
  fiturRN: {
    backgroundColor: "#44A4F4",
    height: 155,
    width: 160,
    borderRadius: 10,
  },
  fiturRNJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  fiturRNDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
  },
  fiturRNImage: {
    marginTop: 3,
    marginLeft: 8,
  },

  // Fitur Java
  fiturJava: {
    backgroundColor: "#000000",
    height: 155,
    width: 160,
    borderRadius: 10,
  },
  fiturJavaJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
  },
  fiturJavaDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
    color: "white",
  },
  fiturJavaImage: {
    marginTop: 33,
    marginLeft: 8,
  },

  // Fitur 3
  fitur3: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
  },
  // Fitur Python
  fiturPython: {
    backgroundColor: "#4C8478",
    height: 155,
    width: 160,
    borderRadius: 10,
  },
  fiturPythonJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
  },
  fiturPythonDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
    color: "white",
  },
  fiturPythonImage: {
    marginTop: 43,
    marginLeft: 8,
  },

  // Fitur Web
  fiturWebDasar: {
    backgroundColor: "#CD0000",
    height: 155,
    width: 160,
    borderRadius: 10,
    color: "white",
  },
  fiturWebDasarJudul: {
    paddingTop: 5,
    paddingLeft: 10,
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
  },
  fiturWebDasarDesc: {
    paddingLeft: 10,
    fontSize: 14,
    paddingTop: 5,
    color: "white",
  },
  fiturWebDasarImage: {
    marginTop: 10,
    marginLeft: 8,
  },

  // Button
  buttonContainer: {
    backgroundColor: "#3FA9D7",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 18,
    height: "100%",
  },
  buttonWidth: {
    width: "33.3%",
  },
  buttonWrap: {
    alignItems: "center",
    marginTop: 14,
  },
});
