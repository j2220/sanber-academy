import { StyleSheet, Text, View, Image } from "react-native";
import React, { useEffect } from "react";
import { StackActions } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";

const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.dispatch(StackActions.replace("Login"));
    }, 2000);
  }, []);
  return (
    <View style={styles.container}>
      <Image
        source={require("../assets/sanbecode-logo-lite.png")}
        style={{ width: 280, height: 150 }}
      />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
