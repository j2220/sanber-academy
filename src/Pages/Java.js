import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'

const Java = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30, color: "white" }}>
          {" "}
          Java
        </Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text
          style={{
            textAlign: "justify",
            fontSize: 18,
            lineHeight: 30,
            color: "white",
          }}
        >
          Java adalah bahasa pemrograman yang biasa digunakan untuk
          mengembangkan bagian back-end dari software, aplikasi Android, dan
          juga website. Java juga dikenal memiliki moto “Write Once, Run
          Anywhere”. Java memiliki sistem syntax atau kode pemrograman level
          tinggi. Di mana ketika dijalankan, syntax akan di-compile dengan Java
          Virtual Machine (JVM) menjadi kode numeric (bytescode) platform.
          Sehingga aplikasi Java bisa dijalankan di berbagai perangkat.
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={require("../assets/java.png")}
          style={{ width: 100, height: 100 }}
        />
      </View>
    </View>
  );
};

export default Java

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000",
  },
});