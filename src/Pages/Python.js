import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const Python = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30, color: "white" }}>
          {" "}
          Python
        </Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text
          style={{
            textAlign: "justify",
            fontSize: 18,
            lineHeight: 30,
            color: "white",
          }}
        >
          Python adalah bahasa pemrograman interpretatif yang dapat digunakan di
          berbagai platform dengan filosofi perancangan yang berfokus pada
          tingkat keterbacaan kode dan merupakan salah satu bahasa populer yang
          berkaitan dengan Data Science, Machine Learning, dan Internet of
          Things (IoT). Keunggulan Python yang bersifat interpretatif juga
          banyak digunakan untuk prototyping, scripting dalam pengelolaan
          infrastruktur, hingga pembuatan website berskala besar.
        </Text>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={require("../assets/python.png")}
          style={{ width: 100, height: 100 }}
        />
      </View>
    </View>
  );
};

export default Python

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4C8478",
  },
});