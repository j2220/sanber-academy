import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import Login from "./Login";
import { StackActions } from "@react-navigation/native";

const Profile = ({navigation}) => {
   const firebaseConfig = {
     apiKey: "AIzaSyAhp86E3z2R8qOcwzlqI1c_HnmxYbbrim8",
     authDomain: "authfirebasern-1fa10.firebaseapp.com",
     projectId: "authfirebasern-1fa10",
     storageBucket: "authfirebasern-1fa10.appspot.com",
     messagingSenderId: "363816926304",
     appId: "1:363816926304:web:6779a82537bc5aedd8d705",
     measurementId: "G-05WLEJ7027",
   };

   if (!firebase.apps.length) {
     firebase.initializeApp(firebaseConfig);
   }

   const logOut = () => {
     firebase
       .auth()
       .signOut()
       .then((test) => {
         console.log("Logout Berhasil");
         console.log(test);
         alert("Logout Berhasil");
        //  navigation.navigate("Login");
           navigation.dispatch(StackActions.replace("Login"));
         console.log("user id: " + firebase.auth().currentUser.uid);
       })
       .catch((error) => {
         console.log(error);
       });
   };
  return (
    <View style={styles.container}>
      {/* Header */}
      <View style={styles.header}>
        {/* <View style={styles.headerImage}>
          <Image
            source={require("./assets/sanbecode-logo-lite.png")}
            style={{ width: 110, height: 65 }}
          />
        </View> */}
        <TouchableOpacity
          onPress={logOut}
          style={{
            marginLeft: 280,
            borderWidth: 2,
            backgroundColor: "#3FA9D7",
            borderColor: "#3FA9D7",
            marginTop: 0,
            alignItems: "center",
            padding: 6,
            borderRadius: 15,
          }}
        >
          <Text style={{ color: "white" }}>Log Out</Text>
        </TouchableOpacity>
        <View>
          <Text style={styles.headerText}>Profile</Text>
        </View>
      </View>
      {/* Foto dan Nama */}
      <View style={styles.profil}>
        <View style={{ marginRight: 13, marginTop: 10 }}>
          <Text style={{ fontSize: 20 }}>Arief Rachman Hakim</Text>
          <Text style={{ fontSize: 12, marginLeft: 66 }}>
            React Native Developer
          </Text>
        </View>
        <View>
          <Image
            source={require("../assets/user-profile.png")}
            style={{ width: 70, height: 70, borderRadius: 50 }}
          />
        </View>
      </View>
      {/* Git &Github */}
      <View style={styles.portofolio}>
        <View style={{ alignItems: "center", marginTop: 10 }}>
          <Text style={{ fontSize: 24 }}>Portofolio</Text>
        </View>
        {/* Icon Github & Gitlab */}
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 30,
          }}
        >
          <View style={{ flexDirection: "row", marginTop: -10 }}>
            <Image source={require("../assets/github.png")} />
            <View style={{ marginLeft: 15, marginTop: -2 }}>
              <Text style={{ fontSize: 20 }}>Github</Text>
              <Text style={{ fontSize: 12, marginTop: -2 }}>@ariefhk</Text>
            </View>
          </View>
          <View
            style={{
              backgroundColor: "black",
              marginTop: 6,
              width: 100,
              height: 3,
              transform: [{ rotate: "90deg" }],
            }}
          />
          <View
            style={{
              flexDirection: "row",
              marginLeft: -20,
              alignItems: "center",
              marginTop: -10,
            }}
          >
            <Image source={require("../assets/gitlab.png")} />
            <View style={{}}>
              <Text style={{ fontSize: 20 }}>Gitlab</Text>
              <Text style={{ fontSize: 12, marginTop: -2 }}>@ariefhk</Text>
            </View>
          </View>
        </View>
      </View>
      {/* Contact */}
      <View
        style={{
          backgroundColor: "#3FA9D7",
          height: 168,
          marginLeft: 16,
          marginRight: 16,
          marginTop: 29,
          borderRadius: 16,
        }}
      >
        <View style={{ alignItems: "center", marginTop: 10 }}>
          <Text style={{ fontSize: 24 }}>Contact</Text>
        </View>
        <View
          style={{
            backgroundColor: "black",
            marginTop: 6,
            width: 300,
            height: 3,
            marginLeft: 32,
          }}
        />
        {/* Icon Github & Gitlab */}
        <View
          style={{
            marginLeft: 30,
            marginTop: 30,
          }}
        >
          {/* WA */}
          <View
            style={{
              flexDirection: "row",
              marginTop: -18,
              alignItems: "center",
            }}
          >
            <Image source={require("../assets/whatsapp.png")} />
            <View style={{ marginLeft: 15, marginTop: -2 }}>
              <Text style={{ fontSize: 14 }}>082240136816</Text>
            </View>
          </View>
          {/* IG */}
          <View
            style={{
              flexDirection: "row",
              marginTop: 8,
              alignItems: "center",
            }}
          >
            <Image source={require("../assets/instagram.png")} />
            <View style={{ marginLeft: 15, marginTop: -2 }}>
              <Text style={{ fontSize: 14 }}>@ariefrhk_</Text>
            </View>
          </View>
          {/* Gmail */}
          <View
            style={{
              flexDirection: "row",
              marginTop: 8,
              alignItems: "center",
            }}
          >
            <Image source={require("../assets/gmail.png")} />
            <View style={{ marginLeft: 15, marginTop: -2 }}>
              <Text style={{ fontSize: 14 }}>
                ariefrachmanhakim1001@gmail.com
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    // flexDirection: "row",
    marginTop: 60,
    // marginLeft: -100,
    alignItems: "center",
    justifyContent: "center",
  },
  headerImage: {
    marginTop: 5,
  },
  headerText: {
    fontSize: 30,
    marginTop: 12,
    // alignItems: "center"
  },
  profil: {
    flexDirection: "row",
    marginTop: 30,
    marginLeft: 100,
  },
  portofolio: {
    backgroundColor: "#3FA9D7",
    height: 168,
    marginLeft: 16,
    marginRight: 16,
    marginTop: 16,
    borderRadius: 16,
  },
});
