import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'

const JavaScript = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={{ marginTop: 80 }}>
        <Text style={{ textAlign: "center", fontSize: 30 }}> JavaScript</Text>
      </View>
      <View style={{ margin: 30 }}>
        <Text style={{ textAlign: "justify", fontSize: 18, lineHeight: 30 }}>
          JavaScript adalah bahasa pemrograman yang digunakan dalam pengembangan
          website agar lebih dinamis dan interaktif. Kalau sebelumnya kamu hanya
          mengenal HTML dan CSS, nah sekarang kamu jadi tahu bahwa JavaScript
          dapat meningkatkan fungsionalitas pada halaman web. Bahkan dengan
          JavaScript ini kamu bisa membuat aplikasi, tools, atau bahkan game
          pada web.
        </Text>
      </View>
      <View style={{justifyContent: "center", alignItems:"center"}}>
        <Image
          source={require("../assets/javascript.png")}
          style={{width: 100, height: 100}}
        />
      </View>
    </View>
  );
}

export default JavaScript

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F4DC1C",
  },
});