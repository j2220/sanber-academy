import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  TextInput,
} from "react-native";
import React, { useState, useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { StackActions } from "@react-navigation/native";


const Login = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const firebaseConfig = {
    apiKey: "AIzaSyAhp86E3z2R8qOcwzlqI1c_HnmxYbbrim8",
    authDomain: "authfirebasern-1fa10.firebaseapp.com",
    projectId: "authfirebasern-1fa10",
    storageBucket: "authfirebasern-1fa10.appspot.com",
    messagingSenderId: "363816926304",
    appId: "1:363816926304:web:6779a82537bc5aedd8d705",
    measurementId: "G-05WLEJ7027",
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }
  const submit = () => {
    const data = { email, password };
    console.log(data);
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        console.log("Login Berhasil");
        alert("Login Berhasil");
        // navigation.navigate("MainApp");
          navigation.dispatch(StackActions.replace("MainApp"));
        console.log("user id: " + firebase.auth().currentUser.uid);
      })
      .catch(() => {
        alert("Login Gagal");
      });
  };
  return (
    <View style={styles.container}>
      {/* Gambar */}
      <View style={styles.header}>
        <Image source={require("../assets/sanber-code.png")} />
        <Text style={styles.headerText}>Academy</Text>
      </View>
      {/* Form */}
      <View style={styles.form}>
        <View style={styles.formContainer}>
          <Text style={{ color: "#073665", fontSize: 24 }}>Masuk</Text>
        </View>

        <TextInput
          style={styles.formUser}
          placeholder="Username/Email"
          value={email}
          onChangeText={(value) => {
            setEmail(value);
          }}
        />

        <TextInput
          secureTextEntry={true}
          style={styles.formPassword}
          placeholder="Password"
          value={password}
          onChangeText={(value) => {
            setPassword(value);
          }}
        />
        <View
          style={{
            marginTop: -14,
            paddingVertical: 10,
            width: 300,
          }}
        >
          <TouchableOpacity>
            <Text style={{ fontSize: 13, marginLeft: 6 }}>
              Belum mempunyai Akun ? Klik{" "}
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 16,
                  textAlign: "center",
                }}
              >
                Daftar
              </Text>
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* Button */}
      <View style={styles.button}>
        <View>
          <TouchableOpacity onPress={submit} style={styles.buttonMasuk}>
            <Text style={{ color: "#fff", textAlign: "center", fontSize: 20 }}>
              Masuk
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity
            style={styles.buttonKeluar}
            onPress={() => {
              navigation.navigate("Register");
            }}
          >
            <Text
              style={{ color: "#517DA2", textAlign: "center", fontSize: 20 }}
            >
              Daftar
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
  },

  headerText: {
    color: "#073665",
    fontWeight: "bold",
    fontSize: 30,
    marginTop: -20,
  },
  form: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  formContainer: {
    marginTop: 43,
    paddingVertical: 10,
    width: 300,
    marginLeft: 6,
  },
  formUser: {
    borderWidth: 1,
    paddingVertical: 10,
    borderRadius: 15,
    width: 300,
    marginBottom: 10,
    paddingHorizontal: 10,
    fontSize: 18,
  },
  formPassword: {
    marginTop: 8,
    borderWidth: 1,
    paddingVertical: 10,
    borderRadius: 15,
    width: 300,
    marginBottom: 10,
    paddingHorizontal: 10,
    fontSize: 18,
  },
  button: {
    marginTop: 50,
  },
  buttonMasuk: {
    marginRight: 80,
    marginLeft: 80,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#517DA2",
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#fff",
  },
  buttonKeluar: {
    marginRight: 80,
    marginLeft: 80,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "#fff",
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#517DA2",
  },
});
