import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Router from './Router/Index'
import Login from './Pages/Login'
import Register from './Pages/Register'
import Learn from './Pages/Learn'
import Profile from './Pages/Profile'
import SplashScreen from './Pages/SplashScreen'
import Chat from './Pages/Chat'
import JavaScript from './Pages/JavaScript'
import Flutter from './Pages/Flutter'
import ReactNative from './Pages/ReactNative'
import Java from './Pages/Java'
import Python from './Pages/Python'
import WebDasar from './Pages/WebDasar'

const Index = () => {
  return (
    <Router/>
    // <Login/>
    // <Register/>
    // <Learn/>
    // <Profile/>
    // <SplashScreen/>
    // <Chat/>
    // <JavaScript/>
    // <Flutter/>
    // <ReactNative/>
    // <Python/>
    // <Java/>
    // <WebDasar/>
  )
}

export default Index

const styles = StyleSheet.create({})